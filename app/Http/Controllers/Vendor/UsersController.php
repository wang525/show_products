<?php

namespace App\Http\Controllers\Vendor;

use App\Http\Controllers\Controller;
use Auth;

class UsersController extends Controller
{

    public function logout()
    {
        Auth::logout();
        return redirect('/');
    }

}
